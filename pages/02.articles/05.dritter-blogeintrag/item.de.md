---
title: 'Dritter Blogeintrag'
date: '12-04-2022 00:00'
authors:
    Tuk: 'Pronomen er, ist weiß positioniert und seit mehreren Jahren in der Klimagerechtigkeitsbewegung aktiv. Außerdem ist er in Berlin in einer migrantischen Selbstorganisierung aktiv und arbeitet zu (Wohn)Raum in der Stadt.'
    Mara: 'Pronomen sie, ist weiß und seit ein paar Jahren in der Klimagerechtigkeitsbewegung aktiv. Daneben ist sie in queer-feministischen und antirassistischen Gruppen unterwegs. Mara ist seit Anfang 2020 bei Decolonize Climate Action!.'
    Sigrid: 'ist cool.'
sources:
    - '<a href=''https://www.youtube.com/watch?v=CPa_fzPr1m8''>https://www.youtube.com/watch?v=CPa_fzPr1m8</a>, abgerufen am 23.4.2022'
    - 'Adorno, Theodor Wiesengrund: Ästhetische Theorie, 7. Aufl., Frankfurt am Main: Suhrkamp, 1970, S. 9'
---

Hallo
