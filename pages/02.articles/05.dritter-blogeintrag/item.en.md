---
title: 'Dritter Blogeintrag'
date: '03-05-2022 00:00'
authors:
    Tuk: 'Pronomen er, ist weiß positioniert und seit mehreren Jahren in der Klimagerechtigkeitsbewegung aktiv. Außerdem ist er in Berlin in einer migrantischen Selbstorganisierung aktiv und arbeitet zu (Wohn)Raum in der Stadt.'
    Mara: 'Pronomen sie, ist weiß und seit ein paar Jahren in der Klimagerechtigkeitsbewegung aktiv. Daneben ist sie in queer-feministischen und antirassistischen Gruppen unterwegs. Mara ist seit Anfang 2020 bei Decolonize Climate Action!.'
    Sigrid: 'ist cool.'
sources:
    - '<a href=''https://www.youtube.com/watch?v=CPa_fzPr1m8''>https://www.youtube.com/watch?v=CPa_fzPr1m8</a>, abgerufen am 23.4.2022'
    - '<a href=''https://www.youtube.com/watch?v=CPa_fzPr1m8''>Klicke hier</a>, abgerufen am 23.4.2022'
    - 'Adorno, Theodor Wiesengrund: Ästhetische Theorie, 7. Aufl., Frankfurt am Main: Suhrkamp, 1970, S. 9'
publish_date: '03-05-2022 19:35'
media_order: 'Probe-Bild 3.jpg'
---

Hello :)

===


![Alternativtext Roots of resistance](https://www.ende-gelaende.org/wp-content/uploads/2022/04/ROR-Website-1-400x200.jpg)
![Climate Justice Now ist gesprayt.](Probe-Bild%203.jpg "Probe-Bild%203")

![Sample Image](Probe-Bild%203.jpg?lightbox=600,400&resize=200,200)