---
title: Testseite
---

# Blog Release - alles was ihr wissen müsst!

> Zitat test so könnte das aussehen wenn wir eine Zitat besonders herausstellen wollen.

_Stell dir vor, du fährst zu einem mehrtägigen Event der deutschsprachigen Klimagerechtigkeitsbewegung. Du freust dich und bist gespannt, mit welchen coolen Menschen du die nächsten Tage diskutieren und planen wirst. Du kommst an, betrittst den Raum und lässt das geschäftige Treiben erstmal auf dich wirken. Es überfordert dich schon immer wieder ein bisschen, so viele neue Menschen zu sehen, zu versuchen, sie einzuschätzen und einen Überblick zu bekommen, was eigentlich wo gerade passiert. Du schaust dich um.
Fällt dir auf, dass fast alle Menschen im Raum weiß sind? Was meinst du, woran liegt das?
Wie fühlst du dich, als es dir auffällt? Sprichst du mit anderen darüber?_

_Im Laufe des Abends werden viele Ideen und Strategien auf den Tisch gepackt, was die Klimagerechtigkeitsbewegung in Deutschland tun sollte. Gruppen tauschen ihre Pläne aus. Du spürst Motivation, dich auch in die Planung des Jahres zu stürzen. Fällt dir auf, dass auf dem Podium bisher nicht über Rassismus und Kolonialismus gesprochen wurde? Nicht über Antisemitismus und über viele andere Strukturen gesellschaftlicher Unterdrückung, die doch so eng mit der Klimakrise verbunden sind. Später am Tag geht eine Person of Color auf die Bühne und kritisiert, dass die weiß-dominierten Gruppen der Bewegung ihrer Aufgabe seit Jahren nicht nachkommen, sich (selbstkritisch) mit Rassismus und Kolonialismus zu beschäftigen.
Welche Konsequenzen ziehst du daraus?_

Es ist nicht das erste Mal, dass die Klimagerechtigkeitsbewegung in Deutschland für ihre fehlende Sensibilität gegenüber Rassismus in den eigenen Reihen und fehlende Beachtung nicht-weißer Perspektiven kritisiert wird. Doch selbstkritische Lernprozesse oder gar die antirassistische Ausrichtung von Aktionen verlaufen schleppend. Auch Antisemitismus wird zu wenig aufgearbeitet und oft gar nicht erst erkannt und koloniale Kontinuitäten finden zu wenig Eingang in die Analysen und Forderungen.

Dabei sind sie so wichtig! Es gibt tausend Gründe, sich gegen Rassismus, Antisemitismus und alle anderen Formen der Unterdrückung einzusetzen, nicht zuletzt, weil es moralisch-ethisch richtig ist. Einen Grund möchten wir hier extra beleuchten: Die Klimakrise ist in ihrer Entstehung eng mit dem Kolonialismus verwoben. Um die Klimakrise einzudämmen, müssen wir die kolonialen Strukturen der Gesellschaft zerschlagen. Dafür müssen wir Bewegungen aufbauen, in denen sich Menschen mit verschiedenen Erfahrungen wohlfühlen, und zwar gerade jene Menschen, die Formen der Unterdrückung, wie Rassismus oder Antisemitismus ausgesetzt sind. Eine Bewegung, die BI_PoCs und jüdische Menschen ausschließt, wird nie stark genug sein, die notwendigen gesellschaftlichen Veränderungen umzusetzen, die es braucht, um die Klimakrise einzudämmen und um einen gerechten Umgang mit ihren Folgen zu finden.
Menschen sind unterschiedlich von den Klimawandel-Folgen betroffen und haben unterschiedlich viele Ressourcen, mit den Folgen umzugehen. Außerdem sind die verschiedenen Formen von Unterdrückung eng miteinander und dem Kapitalismus verflochten und stützen und bedingen sich gegenseitig, weshalb wir sie auch nur gemeinsam überwinden werden.

Wie die Klimakrise mit Kolonialismus verflochten ist und wieso sie sich nicht eindämmen lassen wird, ohne Rassismus und koloniale Strukturen anzugehen, wird zum Beispiel in der Broschüre "Kolonialismus & Klimakrise - über 500 Jahre Widerstand" von der BUNDjugend sehr gut erklärt - schaut sie euch an. Auch in den folgenden Beiträgen auf dieser Website möchten wir uns dieses und die anderen angesprochenen Themen und Verschränkungen genauer anschauen.

## Wer steckt hinter dieser Website?

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

### Die Nächste h3

lorem ipsum und so weiter blabla

#### Jetzt wirds kleiner h4

lorem ipsum und so weiter blabla

##### Zweitkleinste Überschrift h5

lorem ipsum und so weiter blabla
lorem ipsum und so weiter blabla

###### Miniüberschrift h6

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.