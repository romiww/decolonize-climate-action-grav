---
title: 'About us'
content:
    items:
        - '@self.children'
    limit: 5
    order:
        by: date
        dir: desc
    pagination: true
    url_taxonomy_filters: true
---

### About us

Here we want to publish a text about our group and how we work. It is not ready to be published yet.