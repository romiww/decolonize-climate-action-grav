---
title: 'Über uns'
content:
    items:
        - '@self.children'
    limit: 5
    order:
        by: date
        dir: desc
    pagination: true
    url_taxonomy_filters: true
---

### Über uns

Unser Selbstverständnis ist noch in Arbeit.