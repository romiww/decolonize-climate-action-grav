---
title: 'Short Description'
menu: Selbstverständnis
image_align: left
---

## DECOLONIZE CLIMATE ACTION!

This website is a space for debate. It is a **platform** for **reflecting upon and fighting against colonial structures, racism and anti-Semitism within the climate justice movement and the wider climate discourse in Germany**. It is a space to criticize, inform, listen, discuss, dissent and reflect, to network and to change. 
We ask ourselves the questions: How can we concretely bring about change within the climate justice movement? What is already happening and how is it going? What debates are currently taking place in the movement?
You can find texts, comics and poems, soon videos (hopefully), as well as collections of material and a collection of groups that deal with these issues.

We kindly invite people that are critically engaged with topics around anti-racism and anti-Semitism and especially people and initiatives that are affected by racism and anti-Semitism to use this website as a platform for demands, thoughts, analyses and criticism. We are happy to publish your text, poem, comic, video etc.. 
For information about publishing feel free to contact us: decolonizeclimateaction (at) riseup (.) net.

This website is moderated by the group **_Decolonize Climate Action! - No climate justice without decolonization!_** If you want to become a part of us, just send us an email: decolonizeclimateaction (at) riseup (.) net. We look forward to hear from you.