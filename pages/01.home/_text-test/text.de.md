---
title: 'Kurze Beschreibung'
menu: Selbstverständnis
image_align: left
---

## DECOLONIZE CLIMATE ACTION!

Diese Website ist ein Raum für Auseinandersetzung. Sie ist eine **Plattform** zur **Aufarbeitung und Bekämpfung von kolonialen Strukturen, Rassismus und Antisemitismus in der Klimagerechtigkeitsbewegung und im Klimadiskurs in Deutschland**. Sie ist ein Raum zum Kritisieren, Informieren, Zuhören, Diskutieren, Widersprechen und Reflektieren, zum Vernetzen und Verändern. Wir stellen uns die Fragen: Wie kann Veränderung innerhalb der Klimagerechtigkeitsbewegung konkret bewirkt werden? Was passiert bereits und wie läuft es? Welche Auseinandersetzungen finden aktuell in der Bewegung statt?
Ihr findet hier Texte, hoffentlich auch bald Videos, Comics und Gedichte, sowie eine Materialsammlung und eine Sammlung von Gruppen, die sich mit ebendiesen Themen beschäftigen.

Ganz herzlich laden wir antirassistische, antisemitismuskritische und insbesondere selbst von Rassismus und Antisemitismus betroffene Menschen und Initiativen ein, diese Website als Plattform für Forderungen, Gedanken, Analysen und Kritik zu nutzen. Wir veröffentlichen Deinen/Euren Text, Dein/Euer Gedicht, Comic, Video etc. sehr gerne. 
Für Infos zur Veröffentlichung schreib uns gerne: decolonizeclimateaction (at) riseup (.) net.

Moderiert wird diese Website von der Gruppe **_Decolonize Climate Action! - Keine Klimagerechtigkeit ohne Dekolonisierung!_** Wenn ihr Teil von uns werden möchtet, schreibt uns gerne eine Email: decolonizeclimateaction (at) riseup (.) net. Wir freuen uns!