---
title: quote-test
menu: Start
quote: Die Klimabewegung in Deutschland ist ebenso rassistisch wie der Rest der Gesellschaft. Aber ich denke, als eine Bewegung ist es unsere Verantwortung und unsere Pflicht, wenn wir die Gesellschaft verändern wollen, es besser zu machen als der Rest der Gesellschaft.
source: Tonny Nowshin
---